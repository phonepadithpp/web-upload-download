const express = require('express');
const multer = require('multer');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');

const app = express();
const port = 3000;

// Connect to SQLite database
const db = new sqlite3.Database('logs.db');

// Create table to store logs if it doesn't exist
db.run(`CREATE TABLE IF NOT EXISTS logs (
  id INTEGER PRIMARY KEY,
  type TEXT,
  filename TEXT,
  timestamp DATETIME,
  duration INTEGER
)`);

// Set storage for file uploads
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
});

const upload = multer({ storage: storage });

// Serve static files from the 'public' directory
app.use(express.static('public'));

app.get('/', (req, res) => {
  // Fetch list of files from the 'public/uploads' directory
  const fs = require('fs');
  fs.readdir('./public/uploads/', (err, files) => {
    if (err) {
      console.error('Error reading directory:', err);
      files = [];
    }
    // Fetch logs from the database, including duration
    db.all("SELECT * FROM logs WHERE type='upload' ORDER BY timestamp DESC", (err, uploadLogs) => {
      if (err) {
        console.error('Error fetching upload logs:', err);
        uploadLogs = [];
      }
      db.all("SELECT * FROM logs WHERE type='download' ORDER BY timestamp DESC", (err, downloadLogs) => {
        if (err) {
          console.error('Error fetching download logs:', err);
          downloadLogs = [];
        }
        res.render('index', { files: files, uploadLogs: uploadLogs, downloadLogs: downloadLogs });
      });
    });
  });
});

// Handle file upload
app.post('/upload', (req, res) => {
  const startTime = Date.now(); // Record start time
  const upload = multer({ 
    storage: storage,
    limits: { fileSize: 7000 * 1024 * 1024 } // Example limit: 10MB
  }).single('file');

  upload(req, res, (err) => {
    if (err) {
      console.error('Error uploading file:', err);
      res.status(500).send('Error uploading file');
      return;
    }
    const endTime = Date.now(); // Record end time
    const duration = endTime - startTime; // Calculate duration
    // Log the upload time and duration
    const timestamp = new Date().toISOString();
    const filename = req.file.filename;
    db.run("INSERT INTO logs (type, filename, timestamp, duration) VALUES (?, ?, ?, ?)", ['upload', filename, timestamp, duration], (err) => {
      if (err) {
        console.error('Error logging upload:', err);
      }
      res.redirect('/');
    });
  });
});

// Handle file download
// Handle file download
app.get('/download/:file', (req, res) => {
  const startTime = Date.now(); // Record start time
  // Log the download time
  const timestamp = new Date().toISOString();
  const filename = req.params.file;
  db.run("INSERT INTO logs (type, filename, timestamp) VALUES (?, ?, ?)", ['download', filename, timestamp], (err) => {
    if (err) {
      console.error('Error logging download:', err);
    }
    const file = `${__dirname}/public/uploads/${filename}`;
    res.download(file, () => {
      const endTime = Date.now(); // Record end time
      const duration = endTime - startTime; // Calculate duration
      // Update the duration in the logs table
      db.run("UPDATE logs SET duration = ? WHERE type = 'download' AND filename = ?", [duration, filename], (err) => {
        if (err) {
          console.error('Error updating download duration:', err);
        }
      });
    });
  });
});



// Handle file deletion
app.get('/delete/:file', (req, res) => {
  const filename = req.params.file;

  // Delete file from the file system
  const filePath = path.join(__dirname, 'public', 'uploads', filename);
  fs.unlink(filePath, (err) => {
    if (err) {
      console.error('Error deleting file:', err);
      res.status(500).send('Error deleting file');
      return;
    }

    // Remove corresponding entry from the database
    db.run("DELETE FROM logs WHERE type = 'upload' AND filename = ?", [filename], (err) => {
      if (err) {
        console.error('Error deleting file from database:', err);
        res.status(500).send('Error deleting file from database');
        return;
      }

      // Redirect back to the homepage
      res.redirect('/');
    });
  });
});



// Set view engine to EJS
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
