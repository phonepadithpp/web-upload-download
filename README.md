# Installation Instructions

Follow these steps to install and run the application:

## Prerequisites
- Node.js (v14 or higher)
- npm (Node Package Manager)
- Docker (if running Dockerized version)

## Installation Steps

1. Clone the repository to your local machine:

    ```
    git clone https://gitlab.com/phonepadithpp/web-upload-download.git
    ```

2. Navigate to the project directory:

    ```
    cd your-repo
    ```

3. Install dependencies:

    ```
    npm install
    ```

## Running the Application

### Without Docker

4. Start the application:

    ```
    npm start
    ```

5. Open your web browser and navigate to [http://localhost:3000](http://localhost:3000).

### With Docker

4. Build the Docker image:

    ```
    docker build -t your-app .
    ```

5. Run the Docker container:

    ```
    docker run -p 3000:3000 your-app
    ```

6. Open your web browser and navigate to [http://localhost:3000](http://localhost:3000).

## Additional Notes

- Make sure the required ports (e.g., 3000) are not in use by other applications.
- If you encounter any issues during installation or execution, refer to the project's documentation or open an issue on GitHub.
